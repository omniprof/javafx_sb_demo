/**
 * Sample Skeleton for 'MainFXML.fxml' Controller Class
 */
package com.kenfogel.javafx_sb_demo.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class MainFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="readout"
    private Label readout; // Value injected by FXMLLoader

    @FXML // fx:id="demoSlider"
    private Slider demoSlider; // Value injected by FXMLLoader

    @FXML // fx:id="exitButton"
    private Button exitButton; // Value injected by FXMLLoader

    @FXML
    void exitAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert readout != null : "fx:id=\"readout\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert demoSlider != null : "fx:id=\"demoSlider\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert exitButton != null : "fx:id=\"exitButton\" was not injected: check your FXML file 'MainFXML.fxml'.";

        demoSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            readout.setText("" + newValue.intValue());
        });

    }
}
